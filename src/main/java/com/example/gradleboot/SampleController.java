package com.example.gradleboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping(name = "/hello")
    public String sayHello(){
        return "Hello, How are you? where are you?";
    }
	
	@GetMapping(name = "/branch")
    public String getBranch(){
        return "Release Branch";
    }
	
	@GetMapping(name = "/release")
    public String getRelease(){
        return "Release Branch here";
    }

	@GetMapping(name = "/masterBranch")
    public String getMasterBranch(){
        return "Its a Master branch?";
    }
}
