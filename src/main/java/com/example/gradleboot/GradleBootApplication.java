package com.example.gradleboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleBootApplication.class, args);
	}

}
